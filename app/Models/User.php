<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'gender', 'dob', 'biography', 'image_src'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id','created_at', 'updated_at'];

   
   protected function setNameAttribute($value){
        $this->attributes['name'] = ucfirst($value);
   }

   protected function setDobAttribute($value){
        $this->attributes['dob'] = date("Y-m-d", strtotime($value));
   }
}
