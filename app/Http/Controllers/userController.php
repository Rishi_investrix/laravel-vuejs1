<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use Image;
// use Intervention\Image\Facades\Image as Image;

class userController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
       $users = User::paginate(3);
       $total = User::count(); 
       $temp = ['users'=>$users,'total'=> $total];
       return view('users', $temp);
        
    }

    public function indexForApi()
    {
        
        $users = User::all(); 
        // return response()->json(['users' => $users]);
        return response()->json($users);

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adduser');     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       // dd($request->all());

        $this->validate($request, [
            'name' => 'required|min:5|max:50',
            'phone' => 'alpha_num|required|min:11|max:15',
            'email' => 'email|unique:users|required|max:50',
            'dob' => 'required|date|before:2000/01/01',
            'biography' => 'required|min:10|max:100',
            'image_src' => 'required|image'
            ]);
            
        // file size must be 1 mb. 
        // try to apply in vuejs 
        
        if((($_FILES['image_src']['size']/1000) >= 1024)) {
            $errors['image_src'] = 'Image is too large. Must be less than 1 megabyte.';
            return back()->withInput()->withErrors($errors);
        }

        if(isset($validator) and $validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        
       
        // age should be 18+. calculate from dob. apply on calender selection

        try{
            
            $file = $request->image_src;
            $fileName = time() . '-' . str_random() . "." . $file->getClientOriginalExtension();

            $path = public_path('images/' . $fileName);
            $img = Image::make($file->getRealPath())->save($path);

            $user = new User($request->all());
            $user->image_src = $path;
            $user->save();

            return redirect()->route('users');
        } catch(Exception $e){
            $data['errors'] = $e->getMessage();
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
