# README #


### What is this repository for? ###

* test task (developed in laravel plus vue.js) 
* Version 1

### How do I get set up? ###

* 1) Copy past all files into localhost
* 2) Create a database and add details in .env file.
* 3) Run bash
*       a.	php artisan migrate
* 		b.	php artisan db:seed
* 4) functionality with vue.js view
*		a.	http://localhost/laravel-vuejs/public/
*		b.	http://localhost/laravel-vuejs/public/add-user-vue
* 5) functionality with blade view
*		a.	http://localhost/laravel-vuejs/public/user
* for depencies' detail follow package.json file

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* contact details are provided in mail.